package com.arifhasnat.assignment_1.view

import City
import WeatherInfoShowModel
import WeatherInfoShowModelImpl
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.arifhasnat.assignment_1.R
import com.arifhasnat.assignment_1.databinding.ActivityMainBinding
import com.arifhasnat.assignment_1.viewmodel.WeatherInfoViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding


    private lateinit var model: WeatherInfoShowModel
    private lateinit var viewModel: WeatherInfoViewModel

    private var cityList: MutableList<City> = mutableListOf()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)


        model = WeatherInfoShowModelImpl(applicationContext)
        // initialize ViewModel
        viewModel = ViewModelProviders.of(this).get(WeatherInfoViewModel::class.java)





        setViewClickListener();
        setLiveDataListeners();

        viewModel.getCityList(model)



    }

    private fun setViewClickListener() {


      //  val selectedCityId = cityList[1].id
       // Log.d("dataCity", selectedCityId.toString());
        // View Weather button click listener
//        btn_view_weather.setOnClickListener {
//            val selectedCityId = cityList[spinner.selectedItemPosition].id
//            viewModel.getWeatherInfo(selectedCityId, model) // fetch weather info
//        }
    }


    private fun setLiveDataListeners() {


        viewModel.cityListLiveData.observe(this, object : Observer<MutableList<City>> {
            override fun onChanged(cities: MutableList<City>) {
               // setCityListSpinner(cities)
                Log.d("dataCity", cities.toString());

            }
        })


        viewModel.cityListFailureLiveData.observe(this, Observer { errorMessage ->
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
        })



    }

//    private fun setCityListSpinner(cityList: MutableList<City>) {
//        this.cityList = cityList
//
//        val arrayAdapter = ArrayAdapter(
//            this,
//            R.layout.support_simple_spinner_dropdown_item,
//            this.cityList.convertToListOfCityName()
//        )
//
//        spinner.adapter = arrayAdapter
//    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
         }

}