package com.arifhasnat.assignment_1.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ui.AppBarConfiguration
import com.arifhasnat.assignment_1.R
import com.arifhasnat.assignment_1.database.db.RegisterDatabase
import com.arifhasnat.assignment_1.database.repository.RegisterRepository
import com.arifhasnat.assignment_1.databinding.ActivityLogin2Binding
import com.arifhasnat.assignment_1.databinding.ActivityMainBinding
import com.arifhasnat.assignment_1.viewmodel.LoginViewModel
import com.arifhasnat.assignment_1.viewmodel.factory.LoginViewModelFactory


class LoginActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityLogin2Binding
    private lateinit var loginViewModel: LoginViewModel

//    val binding2: ViewDataBinding? = DataBindingUtil.setContentView(
//        this, R.layout.content_login)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

       // binding = ActivityLogin2Binding.inflate(layoutInflater)
//        val binding2: ViewDataBinding? = DataBindingUtil.setContentView(
//            this, R.layout.content_login)
//        setContentView(binding.root)
//        setSupportActionBar(binding.toolbar)


        binding = DataBindingUtil.setContentView(this, R.layout.activity_login2)

        val toolbar: Toolbar = binding.toolbar
        setSupportActionBar(toolbar)

        val application = requireNotNull(this).application

        val dao = RegisterDatabase.getInstance(application).registerDatabaseDao

        val repository = RegisterRepository(dao)

        val factory = LoginViewModelFactory(repository, application)

        loginViewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        binding.toolbar

        binding.myLoginViewModel = loginViewModel

        binding.lifecycleOwner = this

        loginViewModel.navigatetoRegister.observe(this, Observer { hasFinished->
            if (hasFinished == true){
                Log.i("MYTAG","insidi observe")
                //displayUsersList()

                val intent = Intent(this, RegistrationActivity::class.java)
                startActivity(intent)
                loginViewModel.doneNavigatingRegiter()
            }
        })

        loginViewModel.errotoast.observe(this, Observer { hasError->
            if(hasError==true){
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
                loginViewModel.donetoast()
            }
        })

        loginViewModel.errotoastUsername .observe(this, Observer { hasError->
            if(hasError==true){
                Toast.makeText(this, "User doesnt exist,please Register!", Toast.LENGTH_SHORT).show()
                loginViewModel.donetoastErrorUsername()
            }
        })

        loginViewModel.errorToastInvalidPassword.observe(this, Observer { hasError->
            if(hasError==true){
                Toast.makeText(this, "Please check your Password", Toast.LENGTH_SHORT).show()
                loginViewModel.donetoastInvalidPassword()
            }
        })

        loginViewModel.navigatetoUserDetails.observe(this, Observer { hasFinished->
            if (hasFinished == true){
                Log.i("MYTAG","insidi observe")
                // navigateUserDetails()
                // loginViewModel.doneNavigatingUserDetails()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        })



    }


}