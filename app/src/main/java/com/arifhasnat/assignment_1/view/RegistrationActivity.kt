package com.arifhasnat.assignment_1.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ui.AppBarConfiguration
import com.arifhasnat.assignment_1.database.db.RegisterDatabase
import com.arifhasnat.assignment_1.database.repository.RegisterRepository
import com.arifhasnat.assignment_1.databinding.ActivityRegistration2Binding
import com.arifhasnat.assignment_1.viewmodel.RegisterViewModel
import com.arifhasnat.assignment_1.viewmodel.factory.RegisterViewModelFactory


class RegistrationActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityRegistration2Binding
    private lateinit var registerViewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegistration2Binding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val application = requireNotNull(this).application

        val dao = RegisterDatabase.getInstance(application).registerDatabaseDao

        val repository = RegisterRepository(dao)

        val factory = RegisterViewModelFactory(repository, application)

        registerViewModel = ViewModelProvider(this, factory).get(RegisterViewModel::class.java)

        binding.myViewModel = registerViewModel

        binding.lifecycleOwner = this

        registerViewModel.navigateto.observe(this, Observer { hasFinished->
            if (hasFinished == true){
                Log.i("MYTAG","insidi observe")
                //displayUsersList()
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                registerViewModel.doneNavigating()
            }
        })

        registerViewModel.userDetailsLiveData.observe(this, Observer {
            Log.i("MYTAG",it.toString()+"000000000000000000000000")
        })


        registerViewModel.errotoast.observe(this, Observer { hasError->
            if(hasError==true){
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
                registerViewModel.donetoast()
            }
        })

        registerViewModel.errotoastUsername.observe(this, Observer { hasError->
            if(hasError==true){
                Toast.makeText(this, "UserName Already taken", Toast.LENGTH_SHORT).show()
                registerViewModel.donetoastUserName()
            }
        })





    }

}